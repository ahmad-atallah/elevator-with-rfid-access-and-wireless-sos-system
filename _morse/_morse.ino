 
/*
 * Hardware:
 * 
 * GPIO # 0 is a pullup button so it is inverted 
 * 
 * Read a card using a mfrc522 reader on your SPI interface
 * Pin layout should be as follows (on ESP Huzzah ):
 * Tx  :  Pin 5
 * Rx  :  Pin 4
 * MOSI:  Pin 13 
 * MISO:  Pin 12 
 * SCK :  Pin 14 
 * SS  :  Pin 2
 * RST :  Pin 16 

 */
 
#include <SPI.h>
#include <RFID.h>

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <SoftwareSerial.h>

/*************** WiFi connection**************/
//const char* ssid = "TP-LINK_2F3686";
//const char* password = "47861268";

//const char* ssid = "BME_ESP";
//const char* password = "12345678";
//const char* host = "esp8266fs";


struct connectWiFi {
     String ssid;
     String password;
    //char _encrypt;
};

char* ssid = "";
char* password = "";

int port = 80;
WiFiServer server(port);

/*************** SOS Set up ****************/
#define BUTTON    0
#define SENDBUTTON   15

#define PULSE_THRESHOLD 200
#define LETTER_SEPARATION 500
#define WORD_SEPARATION 1500
#define TRYING_CONNECT  6000

/*************** RFID Configuration ***************/ 
#define SS_PIN 2
#define RST_PIN 16
 
RFID rfid(SS_PIN,RST_PIN);
 
 
int serNum[5];
 
int cards[][5] = {
  {137,89,2,197,23}, // card 1
  {54,131,21,126,222} // card 2
};
 
bool access = false;
/************** Software Serial *******************/
SoftwareSerial swSer(4, 5);



void setup(){
  Serial.begin(9600);
  SPI.begin();
  rfid.init();

  /******Setup WiFi Connection*******/
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  struct connectWiFi * connectMe = (struct connectWiFi *) 
                       malloc(sizeof(struct connectWiFi));
   connectMe = WiFiScanSetup();
  char charbufSSID[50];
  (connectMe -> ssid).toCharArray(charbufSSID,50);
  char charbufPSWD[50];
  (connectMe -> password).toCharArray(charbufPSWD,50);
  WiFi.begin(charbufSSID, charbufPSWD);
  Serial.println(charbufSSID);
  Serial.println(charbufPSWD);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print("Attempting to connect to WEP network, SSID: ");
    Serial.println(charbufSSID);
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(charbufSSID);
  Serial.print("IP address:// ");
  Serial.println(WiFi.localIP());
  server.begin();
  Serial.println("HTTP server started");
  
  pinMode(BUTTON, INPUT_PULLUP);
  pinMode(SENDBUTTON, INPUT);
  initMorse(BUTTON, PULSE_THRESHOLD, LETTER_SEPARATION, WORD_SEPARATION);
  
  swSer.begin(9600);

}

/************** Send SOS via WiFi  by  BUTTON ENABLE  *********/
void _sendSOS(String phrase){
  WiFiClient client = server.available();
  if(client){
    Serial.println("New Client Detected");
      
     if (client.connected()) {
        
        String command = client.readStringUntil('X');
        Serial.println(command[3]);
        //if (command=="GET"){
          // Serial.print("yeah");
          
          //** Send SOS through WiFi and also for Software Serial**//
          if (/*phrase != "" &&*/  digitalRead(SENDBUTTON) == HIGH){
            //Serial.println( phrase );
            //swSer.print(phrase); 
            Serial.println("ok ... sending morse phrase");
            Serial.println("Please wait");
            client.print(phrase);
          }
          else{
            
            Serial.println("No data to be transfered");
            client.println("press the button to send ");
          }
        }
      //}
     
   client.stop();

  }
 
}

/********************* WiFi Scan Setup*****************************/
struct connectWiFi * WiFiScanSetup(){

  struct connectWiFi *networkS = (struct connectWiFi *) 
                       malloc(sizeof(struct connectWiFi));
  int n = 0;
    Serial.println("scan start");
  // WiFi.scanNetworks will return the number of networks found
  n = WiFi.scanNetworks();
  Serial.println("scan done");
  if (n == 0)
    Serial.println("no networks found");
  else
  {
    Serial.print(n);
    Serial.println(" networks found");
    for (int i = 0; i < n; ++i)
    {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      
     // if no WPA encryption print " " ,else print "*" 
      Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE)?" ":"*");
     // delay(10);
    }
  }
  Serial.println("Please Select your WiFi Network");
  /// if you want LCD here //
  delay(4000);
  int selectN = Serial.read() - '0';
  
  if(selectN <= n ){
     networkS -> ssid = WiFi.SSID(selectN - 1);
     Serial.println( networkS -> ssid);

     if(WiFi.encryptionType(selectN - 1) == ENC_TYPE_NONE)
        {
          //networkS -> password = 0;
          return networkS;
        }
     else{
        Serial.println("The WiFi network is encrypted. Enter your password");
        delay(8000);
        networkS -> password = Serial.readString();
        return networkS;

     }
     Serial.println( networkS -> ssid);
          

     }
  
}

/************ Loop - Including RFID and Morse get character in its access  ************/
void loop()
{ 
  //Serial.println(digitalRead(SENDBUTTON));
  if(rfid.isCard()){
        String phrase = "";
        if(rfid.readCardSerial()){
            Serial.print(rfid.serNum[0]);
            Serial.print(",");
            Serial.print(rfid.serNum[1]);
            Serial.print(",");
            Serial.print(rfid.serNum[2]);
            Serial.print(",");
            Serial.print(rfid.serNum[3]);
            Serial.print(",");
            Serial.print(rfid.serNum[4]);
            Serial.println("");
            
            for(int x = 0; x < sizeof(cards); x++){
              for(int i = 0; i < sizeof(rfid.serNum); i++ ){
                  if(rfid.serNum[i] != cards[x][i]) {
                      access = false;
                      break;
                  } else {
                      access = true;
                  }
              }
              if(access) break;
            }
           
        }
        
       if(access){
           
           Serial.println("Welcome");
           while(true){
                // dismiss first spaces // 
               
                // variable to store value in this functionlitiy  - declared static so that it stores
                // values in between function calls, but no other functions can change its 
                static boolean dismiss = true;
                          
                char c = getNextChar();

                if (dismiss) {
                  dismiss = false ; 
                  if (c == ' ')
                    return;
                }
              
                Serial.print(c);
                phrase += c;
                 _sendSOS(phrase);
           }
       

       }
       else {
           Serial.println("Denied!"); 
        }            
      }
 //delay (500);
 rfid.halt();


}

